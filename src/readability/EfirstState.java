package readability;

/**
 * Find character 'e' or 'E' before vowel state. 
 * @author Patinya Yongyai
 *
 */
public class EfirstState implements State{

	/**
	 * Check state of character.
	 * @param c is character
	 * @word is word counter class
	 */
	public void handleChar(char c,WordCounter word) {
		if (isVowel(c))
			setState(word.vowelState,word);
		else if(Character.isLetter(c)){
			setState(word.consonantState,word);
			word.setSyllables(word.getSyllables()+1);
		}
		else if(c == '-'){
			setState(word.dashState,word);
		}
		else if(c == '!' || c == '.' ||c == '?' || c == ';' )
			word.setSentence(word.getSentence()+1);
		 else if(c == '\'')
			 setState(word.eFirstState,word);
		else
			setState(word.nonwordState,word);

	}

}
