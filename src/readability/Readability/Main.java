package readability.Readability;

import java.io.FileNotFoundException;
import java.io.IOException;

import readability.WordCounter;
import readability.WordCounterGUI;

/**
 * Main for run application.
 * @author Patinya Yongyai
 * @version 06.04.2015
 *
 */
public class Main {
	/**
	 *
	 * @param args for receive data to GUI
	 * @throws IOException if found error from input or output
	 */
	public static void main(String[] args) throws IOException {
		if(args.length > 0){
			String directory = args[0];
			WordCounter counter = new WordCounter();
			counter.countWords(directory);
			String format = ("Filename:   %s\nNumber of Syllables:   %d\nNumber of Words:   %d\nNumber of Sentences:   %d\nFlesch Index:   %.1f\nReadability:   %s");
			String text = String.format(format,directory,counter.getTotalSyllables(),counter.getWord(),counter.getSentence(),counter.getIndex(),counter.getReadability());
			System.out.println(text);
		}else{
			WordCounterGUI gui = new WordCounterGUI();
		}
	}

}
