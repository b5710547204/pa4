package readability;

/**
 * State of WordCounter.
 * @author Patinya Yongyai
 * @version 06.04.2015
 *
 */
public interface State {
	/**
	 * 
	 * @param c is character
	 * @param word is WordCounter class
	 */
	public void handleChar(char c,WordCounter word);
	/**
	 * 
	 * @param c is character
	 * @return index of character
	 */
	default boolean isVowel(char c){
		String vowelList = "AEIOUaeiou";
		return vowelList.indexOf(c) >=0;
	}
	/**
	 * Set current state to new state.
	 */
	default void setState( State newState,WordCounter word ) {
		word.state = newState;
	}
}
