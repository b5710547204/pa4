package readability;

/**
 * Character is dash.
 * @author Patinya Yongyai
 *
 */
public class DashState implements State{

	/**
	 * Check state of character.
	 * @param c is character
	 * @word is word counter class
	 */
	public void handleChar(char c,WordCounter word) {
		if (c=='e'||c=='E')
			setState(word.eFirstState,word);
		 else if (isVowel(c) || c == 'y' || c == 'Y')
			 setState(word.vowelState,word);
		 else if(Character.isLetter(c))
			 setState(word.consonantState,word);
		 else if(c == '!' || c == '.' ||c == '?' || c == ';' )
			word.setSentence(word.getSentence()+1);
		 else if(c == '\'')
			 setState(word.dashState,word);
		 else
			 setState(word.nonwordState,word);
		
	}


}
