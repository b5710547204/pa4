package readability;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * GUI for show application.
 * @author Patinya Yongyai
 *
 */
public class WordCounterGUI extends JFrame{
	private JTextField fileName = new JTextField(13);
	private JLabel name_lb = new JLabel("File or URL name: ");
	private JButton browse_bt = new JButton("Browse...");
	private JButton count_bt = new JButton("Count");
	private JButton clear_bt = new JButton("Clear");
	private JPanel top_panel = new JPanel();
	private JPanel bottom_panel = new JPanel();
	private JPanel main_panel = new JPanel();
	private JTextArea reportArea = new JTextArea(10,50);
	
	/**
	 * constructor of GUI word counter.
	 */
	public WordCounterGUI() {
		super("Readability by Patinya Yongyai");
		initComponents();
	}
	
	/**
	 * Components of GUI word counter.
	 */
	public void initComponents(){
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		top_panel.setLayout(new FlowLayout());
		top_panel.add(name_lb);
		top_panel.add(fileName);
		top_panel.add(browse_bt);
		top_panel.add(count_bt);
		top_panel.add(clear_bt);
		
		bottom_panel.setLayout(new FlowLayout());
		bottom_panel.add(reportArea);
		
		main_panel.setLayout(new BoxLayout(main_panel, BoxLayout.Y_AXIS));
		main_panel.add(top_panel);
		main_panel.add(bottom_panel);
		
		/**
		 * ActionListener of browse button.
		 */
		browse_bt.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory( new File("C:/") );
				fc.showOpenDialog(WordCounterGUI.this);
				File fileStr = fc.getSelectedFile();
				if(fileStr != null)
					fileName.setText(fileStr.getAbsolutePath());
				
			}
		});
		
		/**
		 * ActionListener of count button.
		 */
		count_bt.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				WordCounter wordCounter = new WordCounter();
				try {
					wordCounter.countWords(fileName.getText());
					String format = ("Filename:   %s\nNumber of Syllables:   %d\nNumber of Words:   %d\nNumber of Sentences:   %d\nFlesch Index:   %.1f\nReadability:   %s");
					String text = String.format(format,fileName.getText(),wordCounter.getTotalSyllables(),wordCounter.getWord(),wordCounter.getSentence(),wordCounter.getIndex(),wordCounter.getReadability());
					reportArea.setText(text);
				} catch (IOException e1) {
					reportArea.setText("Choose your file or URL.");
				}
				
			}
		});
		
		/**
		 * ActionListener of clear button.
		 */
		clear_bt.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				fileName.setText("");
				reportArea.setText("");
				
			}
		});
		
		super.add(main_panel);
		this.pack();
		this.setVisible(true);
	}

}
