package readability;

/**
 * Character is "A,E,I,O,U,a,e,i,o,u".
 * @author Patinya Yongyai
 *
 */
public class VowelState implements State{

	/**
	 * Check state of character.
	 * @param c is character
	 * @word is word counter class
	 */
	public void handleChar(char c,WordCounter word) {
		if(isVowel(c))
			setState(word.vowelState,word);
		else if (Character.isLetter(c)){
			setState(word.consonantState,word);
			word.setSyllables(word.getSyllables()+1);
		}
		else if(c == '-'){
			setState(word.dashState,word);
			word.setSyllables(word.getSyllables()+1);
		}
		else if(c == '!' || c == '.' ||c == '?' || c == ';')
			word.setSentence(word.getSentence()+1);
		 else if(c == '\'')
			 setState(word.vowelState,word);
		else
			setState(word.nonwordState,word);
	}


}
