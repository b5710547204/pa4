package readability;

/**
 * Character isn't a letter.
 * @author Patinya Yongyai
 *
 */
public class NonwordState implements State{

	/**
	 * Check state of character.
	 * @param c is character
	 * @word is word counter class
	 */
	public void handleChar(char c,WordCounter word) {
		word.setSyllables(0);
	}

}
