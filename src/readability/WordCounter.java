package readability;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Arrays;

/**
 * Count words and syllables from text file.
 * @author Patinya Yongyai
 * @version 06.04.2015
 *
 */
public class WordCounter {
	private int countWord = 0;
	State state;
	State eFirstState = new EfirstState();
	State dashState = new DashState();
	State consonantState = new ConsonantState();
	State nonwordState = new NonwordState();
	State vowelState = new VowelState();
	private int sentence = 0;
	private int syllables = 0;
	private int totalSyllables = 0;

	/**
	 * 
	 * @param word for count syllables of that word
	 * @return syllables of that word
	 */
	public int countSyllables(String word){
		syllables = 0;
		state = new StartState();

		String newWord = word.replaceAll("[\",:()/]", "");
		
		String[] wordArr = newWord.split(" ");
		for(int i = 0;i<wordArr.length;i++){
			countWord++;
			char[] words = wordArr[i].toCharArray();
			for(char c: words){
				state.handleChar(c,this);
			}
			if(state == vowelState || (state == eFirstState && syllables == 0))
				syllables++;
			else if( state == eFirstState) {
				syllables--;
			}
			else if(state == dashState || state == nonwordState)
				syllables = 0;
		}
		return syllables;
	}

	/**
	 * 
	 * @return Index of word
	 */
	public double getIndex(){
		double index = 206.835 - 84.6*(totalSyllables*1.0 / countWord) - 1.015*(countWord*1.0 / sentence );
		return index;
	}
	
	/**
	 * 
	 * @return readability of word
	 */
	public String getReadability(){
		if(getIndex()>100)
			return "4th grade student (elementary school)";
		else if(getIndex()>90 && getIndex() <= 100)
			return ("5th grade student");
		else if(getIndex()>80 && getIndex() <= 90)
			return ("6th grade student");
		else if(getIndex()>70 && getIndex() <= 80)
			return ("7th grade student");
		else if(getIndex()>65 && getIndex() <= 70)
			return ("8th grade student");
		else if(getIndex()>60 && getIndex() <= 65)
			return ("9th grade student");
		else if(getIndex()>50 && getIndex() <= 60)
			return ("High school student");
		else if(getIndex()>30 && getIndex() <= 50)
			return ("College student");
		else if(getIndex()>=0 && getIndex() <=30)
			return ("College graduate");
		else
			return ("Advanced degree graduate");
	}
	
	/**
	 * 
	 * @return syllables of word counter
	 */
	public int getSyllables(){
		return syllables;
	}
	
	/**
	 * 
	 * @return total result of syllables
	 */
	public int getTotalSyllables(){
		return totalSyllables;
	}

	/**
	 * 
	 * @param syllables for set syllables of word counter
	 */
	public void setSyllables(int syllables){
		this.syllables = syllables;
	}

	/**
	 * 
	 * @return sentence of word counter
	 */
	public int getSentence(){
		return sentence;
	}

	/**
	 * 
	 * @param sentence for set sentence in word counter
	 */
	public void setSentence(int sentence){
		this.sentence = sentence;
	}

	/**
	 * Count word.
	 * @param in is input stream
	 * @return word from that text file
	 * @throws IOException if found error from input or output
	 */
	public int countWords(String text) throws IOException{
		InputStream input = null;
		
		if(text.contains("http://")) {
			URL url = new URL(text);
			input = url.openStream();
		}
		else {
			File file = new File(text);
			input = new FileInputStream(file);
		}
		
		BufferedReader buf = new BufferedReader(new InputStreamReader(input));
		int count = 0;
		while(buf.ready()){
			String word = buf.readLine();
			count += countSyllables(word);
		}
		totalSyllables = count;
		return count;
	}

	/**
	 * 
	 * @return word from that text file
	 */
	public int getWord(){
		return countWord;
	}

}
